# Spring Boot Java Web App with Hazelcast

Sample Java Web App
- Create Message on Hazelcast
- Search Message by User


## Java Version

JDK 1.8 is used

Please change the pom.xml if you want to change JDK version


## Hazelcast Connection Properties

Please find Hazelcast connection param from application.properties


## Launch Web App

    $ mvn spring-boot:run

## Build App

    $ mvn clean install

## Build Docker Image

    $ docker build -t chris/springboot-hazel:1.0 .

## Deploy Java App and Hazelcast on Kubernetes

Deploy Spring Boot App

    $ kubectl apply -f k8s/springboot-deployment.yaml

Create Spring Boot App Service

    $ kubectl apply -f k8s/springboot-service.yaml

Deploy Hazelcast

    $ kubectl apply -f k8s/hazelcast-deployment.yaml

Create Hazelcast Service

    $ kubectl apply -f k8s/hazelcast-service.yaml


## Access Java Web App

Open your browser and go to `http://<minikube node ip>:30080`
