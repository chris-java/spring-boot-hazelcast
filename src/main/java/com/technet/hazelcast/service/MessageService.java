// This is MessageService.java
// Usage: Defines CRUD operations of message

package com.technet.hazelcast.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientNetworkConfig;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.UUID;

import com.technet.hazelcast.entity.Message;

@Service
public class MessageService {

    public Map<String, Message> getMessageByUser(final String user) {
        // Trigger hazelcast client and get map under user
        return hazelcastClient().getMap(user);
    }

    public void create(final String user, final String content) {
        // Get distributed map from hazelcast
        IMap<String, Message> messages = hazelcastClient().getMap(user);
        // Create entry
        Message message = new Message();
        message.setId(UUID.randomUUID().toString());
        message.setDate(getDateTime());
        message.setContent(content);
        messages.put(message.getId(), message);
        // Stop hazelcast client
        hazelcastClient().shutdown();
    }

    private String getDateTime() {
        DateTimeFormatter date = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return date.format(now);
    }

    // Get Hazelcast host from application.properties
    @Value(value = "${k8s.hazelcast.service}")
    private String hazelcastHost;

    // Hazelcast Client
    private HazelcastInstance hazelcastClient() {
        // Set hazelcast client network connection
        ClientNetworkConfig network = new ClientNetworkConfig();
        network.addAddress(hazelcastHost);

        // Apply configuration to hazelcast client
        ClientConfig config = new ClientConfig();
        config.setNetworkConfig(network);
        return HazelcastClient.newHazelcastClient(config);
    }
}