// This is Message.java
// Usage: Entity of Message to be cached in hazelcast

package com.technet.hazelcast.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

// Lombok, auto generating getter and setter method for each field
// Please install Lombok plugin on your IDE e.g. intellij
@Getter
@Setter
// Implement Serializable transform entity to byte and can be stored in database
public class Message implements Serializable {

    // id of message
    private String id;

    // author of message
    private String date;

    // content of message
    private String content;

}
