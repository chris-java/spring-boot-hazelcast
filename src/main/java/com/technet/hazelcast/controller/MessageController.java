// This is MessageController.java
// Usage: Controller of message caching web app

package com.technet.hazelcast.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.technet.hazelcast.service.MessageService;

@Controller
public class MessageController {

    @Autowired
    private MessageService messageService;

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/searchMessage")
    public String getMessage(@RequestParam String searchUser, Model model) {
        model.addAttribute("searchUser", searchUser);
        model.addAttribute("userMessages", messageService.getMessageByUser(searchUser));
        return "userMessage";
    }

    @PostMapping("/createMessage")
    public String createMessage(@RequestParam String user, @RequestParam String content){
        messageService.create(user, content);
        return "redirect:/";
    }
}
