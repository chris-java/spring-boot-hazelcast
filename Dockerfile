FROM openjdk:8-jdk-alpine

COPY target/hazelcast-*.jar /opt/app/app.jar

WORKDIR /opt/app
ENTRYPOINT ["java", "-jar", "app.jar"]
